class mapa{
    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){
        this.posicionInicial=[4.563933,-74.090859];
        this.escalaInicial=17;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20,
        };

        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial, this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL, this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }

    colocarMarcador(posicion){
        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);

    }    


    colocarCirculo(posicion, configuracion){
        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor); 
        
    }

    colocarPoligono(posicion, configuracion){
        this.poligonos.push(L.polygon(posicion, configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }
}


let miMapa=new mapa();
miMapa.colocarMarcador([4.563933,-74.090859]);
miMapa.colocarMarcador([4.563891,-74.092573]);
miMapa.colocarMarcador([4.563377,-74.091993]);

miMapa.colocarCirculo([4.563933,-74.090859],{
    color:'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 50
});
miMapa.colocarCirculo([4.565666,-74.091650],{
    color:'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 50
});
miMapa.colocarCirculo([4.566179,-74.089170],{
    color:'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 50
});

miMapa.colocarPoligono([[4.562746,-74.091393], [4.561891,-74.091199], [4.562201,-74.090802]],{
    fillColor: "blue",
    opacity: 0.5,
    color:"blue"
});

miMapa.colocarPoligono([[4.561998,-74.094420], [4.563698,-74.094356], [4.563409,-74.092874]],{
    fillColor: "blue",
    opacity: 0.5,
    color:"blue"
});

miMapa.colocarPoligono([[4.564843,-74.097952], [4.566832,-74.095880], [4.564404,-74.095619]],{
    fillColor: "blue",
    opacity: 0.5,
    color:"blue"
});